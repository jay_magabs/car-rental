<?php
include "../includes/dbLibrary.php";
$db = new dbLibrary;
//$db->column(['prod_id','prod_name'])->table('products')->insert([222,'asdf'])->runQuery();
$sql = $db->select()->from('dealer')->where('status','=','pending')->getAll();
// $sql = $db->select()->from('pending')->getAll();


?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <script type="text/javascript">
	function sureToApprove(id){
			if(confirm("Are you sure you want to Approve this request?")){
				window.location.href ='approve.php?id='+id;
      }
    }
    function sureToDisapprove(id){
      if(confirm("Are you sure you want to Disapprove this request?")){
        window.location.href ='disapprove.php?id='+id;
      }
    }
  </script>

<style>
#top-navigation { float:left; white-space:nowrap; color:#fff; padding-top:15px; padding-left:600px; }
#top-navigation a{ color:#fff; }
#top-navigation span{ color:#dca598; }
#top { height:53px; }
</style>
</head>
<body>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" href="../admin/index.php">Dashboard</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="../admin/dealers.php">Dealers</a>
    </li>
    <li class="nav-item active">
      <a class="nav-link" href="#">Pending</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="../admin/bookings.php">Bookings</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="../admin/services.php">Services</a>
    </li>
  </ul>
  <div id="top">
			<div id="top-navigation">
				Welcome <a href="#"><strong>Administrator</strong></a>!
				<span>|</span>
				<a href="login.php">Log out</a>
			</div>
    </div>
</nav>

<div class="container">
  <h2>Pending Applicants for Dealership</h2>          
  <table class="table table-striped">
    <thead>
      <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Contact</th>
        <th>Email</th>
        <th>Address</th>
        <th>Control</th>
      </tr>
    </thead>
    <tbody>
    <?php 
        foreach($sql as $sqli){
            // print_r($sqli);      
    ?>
      <tr>
      <td><?php echo $sqli->dealer_id;?></td>
        <td><?php echo $sqli->dealer_name;?></td>
        <td><?php echo $sqli->contact;?></td>
        <td><?php echo $sqli->email;?></td>
        <td><?php echo $sqli->area;?></td>
        <td><a href="javascript:sureToApprove(<?php echo $sqli->dealer_id;?>)" class="ico del" name="res">Approve</a> / <a href="javascript:sureToDisapprove(<?php echo $sqli->dealer_id;?>)" class="ico edit" name="res">Disapprove</a></td>
      </tr>
    <?php
          }
    ?>
    </tbody>
  </table>
</div>



</body>
</html>


<!-- <!DOCTYPE html>
<html>
<body>

<h2>Basic HTML Table</h2>

<table style="width:100%">
  <tr>
    <th>Product ID</th>
    <th>Product Name</th> 
  </tr>

  foreach($sql as $sqli){
    // print_r($sqli);
  
<tr>
<td> echo $sqli->dealer_id ?></td>
<td>echo $sqli->dealer_name ?></td>
</tr>

}


</body>
</html> -->


