<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <script type="text/javascript">
	function sureToApprove(id){
			if(confirm("Are you sure you want to Approve this request?")){
				window.location.href ='approve.php?id='+id;
      }
    }
    function sureToDisapprove(id){
      if(confirm("Are you sure you want to remove this service?")){
        window.location.href ='delete_service.php?id='+id;
      }
    }
  </script>

<style>
#top-navigation { float:left; white-space:nowrap; color:#fff; padding-top:15px; padding-left:600px; }
#top-navigation a{ color:#fff; }
#top-navigation span{ color:#dca598; }
#top { height:53px; }
</style>
</head>
<body>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" href="../admin/index.php">Dashboard</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="../admin/dealers.php">Dealers</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="../admin/pending.php">Pending</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="../admin/bookings.php">Bookings</a>
    </li>
    <li class="nav-item active" >
      <a class="nav-link" href="../admin/services.php">Services</a>
    </li>
  </ul>
  <div id="top">
			<div id="top-navigation">
				Welcome <a href="#"><strong>Administrator</strong></a>!
				<span>|</span>
				<a href="login.php">Log out</a>
			</div>
    </div>
</nav>

<div class="container">
  <h2>Add New Service</h2>             
  <table>
 
  <form method="post">
  <div class="form-group">
    <label for="exampleInputEmail1">Service Code:</label>
    <input type="text" class="form-control" name="code" aria-describedby="emailHelp" required>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Service Description:</label>
    <input type="text" class="form-control" name="name" required>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Price:</label>
    <input type="text" class="form-control" name="price" required>
  </div>
  <button onclick="location.href='services.php'">Back</button> 
  </div>
  <span> 
  <button type="submit" name="add" >Submit</button>
  </span>
</form> 

  <?php 
  include "../includes/dbLibrary.php";
  include "../includes/config.php";
  
  $db = new dbLibrary;
  if(isset($_POST['add'])){
    $service_id = $_POST['code'];
    $service_name = $_POST['name'];
    $service_price = $_POST['price'];
    $sql = $db->column(['service_id','service_name','service_price'])->table('services')->insert([$service_id,$service_name,$service_price])->runQuery();
    
    if($sql == TRUE){
      echo "<script type = \"text/javascript\">
		  alert(\"Succesfully Added\");
		  window.location = (\"services.php\")
	    </script>";
    }
  }
  ?>  
  </table>
  <footer style="padding-top:10%;">
  
  </footer>
</div>
</body>
</html>


