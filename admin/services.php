<?php
include "../includes/dbLibrary.php";
$db = new dbLibrary;

$sql = $db->select()->from('services')->getAll();
//$db->column(['prod_id','prod_name'])->table('products')->insert([222,'asdf'])->runQuery();
// $sql = $db->select()->from('dealer')->where('stats','=','pending')->getAll();
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<script type="text/javascript">
	function sureToApprove(id){
			if(confirm("Are you sure you want to remove this request? Service Id: "+id)){
				window.location.href ='delete_service.php?id='+id;
      }
    }
  </script>
<style>
#top-navigation { float:left; white-space:nowrap; color:#fff; padding-top:15px; padding-left:600px; }
#top-navigation a{ color:#fff; }
#top-navigation span{ color:#dca598; }
#top { height:53px; }
</style>
</head>
<body>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" href="../admin/index.php">Dashboard</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="../admin/dealers.php">Dealers</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="../admin/pending.php">Pending</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="../admin/bookings.php">Bookings</a>
    </li>
    <li class="nav-item active" >
      <a class="nav-link" href="../admin/services.php">Services</a>
    </li>
  </ul>
  <div id="top">
			<div id="top-navigation">
				Welcome <a href="#"><strong>Administrator</strong></a>!
				<span>|</span>
				<a href="login.php">Log out</a>
			</div>
    </div>
</nav>

<div class="container">
  <h2>Pending Applicants for Dealership</h2>          
  <table class="table table-striped">
    <thead>
      <tr>
        <th>Service Code</th>
        <th>Service Description</th>
        <th>Content Control</th>
      </tr>
    </thead>
    <tbody>
    <?php 
    // include "../includes/dbLibrary.php";
    // $db = new dbLibrary;

    // $sql = $db->select()->from('services')->getAll();
        foreach($sql as $sqli){
            // print_r($sqli);
    ?>
      <tr>
      <td><form method="post"><input style="background-color:transparent; border: 0px solid;" type="text" name="code" value="<?php echo $sqli->service_id;?>" readonly></td>
        <td><i><?php echo $sqli->service_name;?></i></td>
        <td><input name="delete" type="submit" value="Delete" ></form></td> 
      </tr>
          <?php 
          if(isset($_POST['delete'])){
            $service_id = $_POST['code'];
            
            $query = $db->delete('services')->where('service_id','=',$service_id)->runQuery();
            // $query = $db->column(['service_id','service_name','service_price'])->table('services')->delete($service_id)->runQuery();
            // $query = $db->delete('services')->where('service_id','=',$id)->runQuery();
	          if($sql == TRUE){
		        echo "<script type = \"text/javascript\">
		        alert(\"Succesfully Removed\");
		        window.location = (\"services.php\")
	          </script>";
	            }
          }
          ?>
    <?php
        }
    ?>
    </tbody>
  </table>
  <button onclick="location.href='add_service.php'">Add a service</button>
  <table>
  </table>
  <footer style="padding-top:10%;">
  
  </footer>
</div>



<!-- <div class="container">
  <h2>Services Offered</h2>          
  <table class="table table-striped">
    <thead>
      <tr>
        <th>Service Code</th>
        <th>Service Description</th>
        <th>Content Control</th>
      </tr>
    </thead>
    <tbody>
    <php 
        foreach($sql as $sqli){
            // print_r($sqli);
    ?>
      <tr>
      <td><b><php echo $sqli->service_id;?></b></td>
        <td><i><php echo $sqli->service_name;?></i></td>
        <td><a href="javascript:sureToApprove(<php echo $row['car_id'];?>)" class="ico del">Delete</a><a href="#" class="ico edit">Edit</a></td>
        </tr>
    <php
        }
    ?>
    </tbody>
  </table>
  <button onclick="location.href='add_service.php'">Add a service</button>
  <table>
  </table>
  <footer style="padding-top:10%;">
  
  </footer>
</div> -->
</body>
</html>

