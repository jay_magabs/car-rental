
<?php
include "../includes/dbLibrary.php";
$db = new dbLibrary;
//$db->column(['prod_id','prod_name'])->table('products')->insert([222,'asdf'])->runQuery();
$sql = $db->select()->from('dealer')->where('status','=','dealer')->getAll();
$count = 0;
foreach($sql as $sqli){
    $count = $count + 1;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<style>
#top-navigation { float:left; white-space:nowrap; color:#fff; padding-top:15px; padding-left:600px; }
#top-navigation a{ color:#fff; }
#top-navigation span{ color:#dca598; }
#top { height:53px; }

</style>
</head>
<body>

<!-- <div class="container">  
  <h3>Dela Victoria Car Rentals Administrator</h3>
</div> -->
<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
<ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link active" href="#">Dashboard</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="../admin/dealers.php">Dealers</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="../admin/pending.php">Pending</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="../admin/bookings.php">Bookings</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="../admin/services.php">Services</a>
    </li>
  </ul>
  <div id="top">
			<div id="top-navigation">
				Welcome <a href="#"><strong>Administrator</strong></a>!
				<span>|</span>
				<a href="login.php">Log out</a>
			</div>
    </div>
</nav>

<div class="container">
  <h2>Dashboard</h2>
  <!-- <p>The .card-group class is similar to .card-deck, which creates an <strong>equal height and width</strong> grid of cards.</p>
  <p>However, the .card-group class removes left and right margins between each card.</p>
  <p>In this example we have added extra content to the first card, to make it taller. Notice how the other cards follow.</p>
  <p><strong>Note:</strong> The cards are displayed vertically on small screens (less than 576px), <strong>WITH</strong> top and bottom margin:</p> -->
<div class="card-columns" style="padding-left:20%;">
<ul>
<div class="card bg-light" style="width:250px">
  <img class="card-img-top" src="../admin/image/dealer.png" alt="Card image">
  <div class="card-body">
    <h4 class="card-title">Dealers</h4>
    <p class="card-text"> <?php echo $count?> total members.</p>
    <a href="dealers.php" class="btn btn-primary" style="background-color: white; color: black;">View</a>
  </div>
</ul>

<ul>
  <div class="card bg-light" style="width:250px">
  <img class="card-img-top" src="../admin/image/applicant.png" alt="Card image">
  <div class="card-body" >
    <h4 class="card-title">Pending</h4>
    <p class="card-text">Dealer requests.</p>
    <a href="pending.php" class="btn btn-primary" style="background-color: white; color: black;">View</a>
  </div>
  </ul>

  <ul>
  <div class="card bg-light" style="width:250px">
  <img class="card-img-top" src="../admin/image/book.png" alt="Card image">
  <div class="card-body" >
    <h4 class="card-title">Bookings</h4>
    <p class="card-text">Customer & Dealer records.</p>
    <a href="bookings.php" class="btn btn-primary" style="background-color: white; color: black;">View</a>
  </div>
  </ul>

   <ul>
  <div class="card bg-light" style="width:250px">
  <img class="card-img-top" src="../admin/image/services.png" alt="Card image">
  <div class="card-body" >
    <h4 class="card-title">Services</h4>
    <p class="card-text">List of services</p>
    <a href="services.php" class="btn btn-primary" style="background-color: white; color: black;">View</a>
  </div>
  </ul>
  

</div>
</div>
</body>
</html>


