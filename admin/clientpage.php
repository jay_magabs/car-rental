<?php
include '../includes/config.php';
include "../includes/dbLibrary.php";
$db = new dbLibrary;
$id = $_REQUEST['id'];
$db = new dbLibrary;
$sql = $db->select()->from('client')->where('userID','=',$id)->getAll();

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<style>  
#top-navigation { float:left; white-space:nowrap; color:#fff; padding-top:15px; padding-left:600px; }
#top-navigation a{ color:#fff; }
#top-navigation span{ color:#dca598; }
#top { height:53px; }
</style>
</head>
<body>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" href="../admin/index.php">Dashboard</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="../admin/dealers.php">Dealers</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="../admin/pending.php">Pending</a>
    </li>
    <li class="nav-item active">
      <a class="nav-link" href="../admin/bookings.php">Bookings</a>
    </li>
  </ul>
  <div id="top">
			<div id="top-navigation">
				Welcome <a href="#"><strong>Administrator</strong></a>!
				<span>|</span>
				<a href="login.php">Log out</a>
			</div>
    </div>
</nav>

<div class="container" style="padding-top:40px; padding-left:30%;">
<div class="card" style="width: 18rem;">
  <img class="card-img-top" src="../admin/image/icon.jpg" alt="Card image">
  <div class="card-body">
    <h5 class="card-title">Client Details</h5>
    <?php foreach($sql as $sqli)
    { 
    ?>
    <p class="card-text">
    Client ID:<b><?php echo $sqli->userID?></b>
    <br>
    Client First Name:<b><?php echo $sqli->fName?></b>
    <br>
    Client Last Name:<b><?php echo $sqli->lName?></b>
    <br>
    Client Username:<b><?php echo $sqli->username?></b>
    </p>
    <?php
    }
    ?>
    
    <a href="bookings.php" class="btn btn-primary">Go back</a>
  </div>
</div>
</div>



</body>
</html>
