-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 01, 2019 at 07:29 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `car_rental`
--

-- --------------------------------------------------------

--
-- Table structure for table `service_details`
--

CREATE TABLE `service_details` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `dealer_id` int(11) NOT NULL,
  `scheduleDate` datetime NOT NULL,
  `scheduleTime` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service_details`
--

INSERT INTO `service_details` (`id`, `user_id`, `dealer_id`, `scheduleDate`, `scheduleTime`) VALUES
(61, 10, 5, '2019-11-11 00:00:00', '11:11:00'),
(62, 10, 3, '2019-12-12 00:00:00', '00:12:00'),
(63, 10, 3, '2019-12-12 00:00:00', '00:12:00'),
(64, 10, 7, '2019-12-12 00:00:00', '00:12:00'),
(65, 10, 7, '2019-12-12 00:00:00', '00:12:00'),
(66, 10, 7, '2019-12-12 00:00:00', '00:12:00'),
(67, 10, 7, '2019-12-12 00:00:00', '00:12:00'),
(68, 10, 7, '2019-12-12 00:00:00', '00:12:00'),
(69, 10, 7, '2019-12-12 00:00:00', '00:12:00'),
(70, 10, 7, '2019-12-12 00:00:00', '00:12:00'),
(71, 10, 7, '2019-12-12 00:00:00', '00:12:00'),
(72, 10, 7, '2019-12-12 00:00:00', '00:12:00'),
(73, 10, 7, '2019-12-12 00:00:00', '00:12:00'),
(74, 10, 7, '2019-12-12 00:00:00', '00:12:00'),
(75, 10, 7, '2019-12-12 00:00:00', '00:12:00'),
(76, 10, 7, '2019-12-12 00:00:00', '00:12:00'),
(77, 10, 7, '2019-12-12 00:00:00', '00:12:00'),
(78, 13, 6, '2019-06-21 00:00:00', '11:11:00'),
(79, 13, 6, '2019-06-21 00:00:00', '11:11:00'),
(80, 10, 4, '2019-04-04 00:00:00', '11:11:00'),
(81, 10, 4, '2019-04-04 00:00:00', '11:11:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `service_details`
--
ALTER TABLE `service_details`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `service_details`
--
ALTER TABLE `service_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
