-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 01, 2019 at 07:48 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.2.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cars`
--

-- --------------------------------------------------------

--
-- Table structure for table `dealer`
--

CREATE TABLE `dealer` (
  `dealer_id` int(11) NOT NULL,
  `dealer_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `area` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dealer`
--

INSERT INTO `dealer` (`dealer_id`, `dealer_name`, `username`, `password`, `contact`, `email`, `area`, `status`) VALUES
(103, 'Francis Louie Alolor', 'callmefrancis', '$2y$10$KyGt4Pd6hFPMi5SxAcoso.pVC/m6GxQyuR1itcj.7WzliJrmDnIKq', '2147483647', 'f.louiealolor@gmail.com', 'Cebu', 'dealer'),
(104, 'Schiezka dela Victoria', 'schiezka', '$2y$10$GZd9BTQw3C35saYtJbjWcuz6mHA2Mveso.FO.tZZSZAAO6uj/z5UC', '2147483647', 'schiezkadelavictoria@gmail.com', 'Boracay', 'pending'),
(105, 'Joseph Magabilin', 'joseph', '$2y$10$noN8rLDDApGstlfJvc.FVOymXG8FNbe1ixwlhUXTfu3/J/Q408TzG', '2147483647', 'josephmagabilin@gmail.com', 'Manila', 'pending'),
(106, 'Peter Alao', 'peteralao', '$2y$10$D.4VdxKJviJqigdFLfrYDuG942OlE6nQIN3WbIqj1HLgmGcFjLuS6', '09876652312', 'peteralao@gmail.com', 'Davao', 'pending');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dealer`
--
ALTER TABLE `dealer`
  ADD PRIMARY KEY (`dealer_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dealer`
--
ALTER TABLE `dealer`
  MODIFY `dealer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
