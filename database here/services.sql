-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 01, 2019 at 08:25 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.2.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cars`
--

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `service_id` varchar(255) NOT NULL,
  `service_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`service_id`, `service_name`) VALUES
('svs_bcpcl', 'brake cylinders, pipes check for leaks'),
('svs_bftr', 'black fluid check and replacement'),
('svs_check', 'extensive check for leaks, wear and damage, including steering system and driveshaft'),
('svs_coc', 'clutch operation check'),
('svs_eccd', 'exhaust check for corrosion or damage'),
('svs_hoc', 'handbrake operation check'),
('svs_naf', 'new air filter'),
('svs_nff', 'new fuel filter (diesel engines)'),
('svs_nsp', 'new spark plugs (petrol engines)'),
('svs_ofc', 'oil and filter change'),
('svs_rsl', 'reset service light'),
('svs_rwbc', 'removal of wheels and brakes check'),
('svs_scw', 'suspension check for wears'),
('svs_wbcw', 'wheel bearings checked for wears');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`service_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
