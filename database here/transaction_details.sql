-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 05, 2019 at 07:23 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.2.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cars`
--

-- --------------------------------------------------------

--
-- Table structure for table `transaction_details`
--

CREATE TABLE `transaction_details` (
  `transaction_detail_id` int(50) NOT NULL,
  `service_id` varchar(255) NOT NULL,
  `price` double(15,2) NOT NULL,
  `clicked` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaction_details`
--

INSERT INTO `transaction_details` (`transaction_detail_id`, `service_id`, `price`, `clicked`) VALUES
(147, 'svs_scw', 800.00, ''),
(147, 'svs_nff', 4500.00, ''),
(148, 'svs_nsp', 1200.00, ''),
(148, 'svs_ofc', 1000.00, ''),
(148, 'svs_rsl', 6000.00, ''),
(149, 'svs_nsp', 1200.00, ''),
(149, 'svs_nsp', 1200.00, ''),
(149, 'svs_nsp', 1200.00, ''),
(150, 'svs_nsp', 1200.00, ''),
(150, 'svs_naf', 5000.00, ''),
(150, 'svs_rwbc', 8000.00, ''),
(151, 'svs_nff', 4500.00, ''),
(151, 'svs_rwbc', 8000.00, 'add'),
(151, 'svs_wbcw', 650.00, ''),
(153, 'svs_rsl', 6000.00, ''),
(153, 'svs_bftr', 650.00, ''),
(153, 'svs_rwbc', 8000.00, ''),
(154, 'svs_nff', 4500.00, ''),
(154, 'svs_hoc', 350.00, 'add'),
(154, 'svs_rwbc', 8000.00, 'add'),
(154, 'svs_ofc', 1000.00, 'add'),
(155, 'svs_scw', 800.00, ''),
(156, 'svs_scw', 800.00, ''),
(156, 'svs_wbcw', 650.00, ''),
(157, 'svs_scw', 800.00, ''),
(157, 'svs_hoc', 350.00, ''),
(158, 'svs_rwbc', 8000.00, ''),
(158, 'svs_rwbc', 8000.00, ''),
(158, 'svs_nff', 4500.00, '');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
