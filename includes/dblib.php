<?php


class DBLibrary{

    private $db;
    private $sql;
    private $whereInstanceCounter;
    private $columnCount;
    private $valueBag = [];

    public function __construct(){
        try{
            $this->db = new PDO('mysql:host=127.0.0.1;dbname=cars','root','');
        } catch(PDOException $error){
            echo $error->getMessage();
        }
    }
    public function select(Array $fieldList=null){
        $this->sql .= 'SELECT ';
        
        if($fieldList === null){
             $fieldList = '*';
             $this->sql .= $fieldList;
        } else {
             $contents = count($fieldList)-1;
             $count = 0; 
             foreach($fieldList as $field){
                $this->sql .= $field;  
                if($count < $contents)
                   $this->sql .= ', ';
                   
                $count++;   
             }  
        }

        $this->sql .= ' ';

        return $this;
    }

    public function selectDistinct ($table)
    {
        $this->sql .= 'SELECT DISTINCT' .' ' .$table . ' ';
        return $this;
    }
        

    public function from($table){
        $this->sql .= 'from '.$table;
        return $this;
    }

    public function first()
    {   
        $this->sql .= ' LIMIT 1;';
        $dbStatement = $this->db->prepare($this->sql);
        $dbStatement->execute();

        $recordset = $dbStatement->fetch(PDO::FETCH_OBJ);
        $this->sql = '';
        $this->whereInstanceCounter = 0;
        return $recordset;
    }

    public function get(){

        $this->sql .= ';';
        $dbStatement = $this->db->prepare($this->sql);
        $dbStatement->execute();

        $recordset = $dbStatement->fetch(PDO::FETCH_OBJ);
        $this->sql = '';
        $this->whereInstanceCounter = 0;
        return $recordset;
    }
    
//     public function where($field,$operator,$value){
//         $this->valueBag = [];
//         if($this->whereInstanceCounter < 0){ 
//             $this->sql .= 'and';
//             $this->sql .= $field. ' '.$operator.' '.$value;
//         }
//         else if($this->whereInstanceCounter > 0){ 
//             $this->sql .= 'or';
//             $this->sql .= $field. ' '.$operator.' '.$value;
//         }
//         else{
//             $this->sql .= ' WHERE ' .$field. ' '.$operator. ' '.$value;
//         }
    
//     $this->valueBag[] = $value;
//     $this->whereInstanceCounter++;
//     return $this;
//    }
public function where($field,$operator,$value){
    $this->valueBag = [];

    $value = is_numeric($value) ? $value : "'".$value."'";

    if($this->whereInstanceCounter < 0){ 
        $this->sql .= " AND ";
        $this->sql .= $field. " " .$operator." ".'?';
    }
    else if($this->whereInstanceCounter > 0){ 
        $this->sql .= " OR ";
        $this->sql .= $field. " " .$operator." ".$value;
    }
    else{
        $this->sql .= " WHERE " .$field. " " .$operator. " " .$value;
    }

    $this->valueBag[] = $value;
    $this->whereInstanceCounter++;
    return $this;
}
   public function getAll(){
    $this->sql .= ';';
    $dbStatement =$this->db->prepare ($this->sql);
    $dbStatement->execute($this->valueBag);
  //print_r($this->valueBag);

    $recordset = $dbStatement->fetchAll(PDO::FETCH_OBJ);
    $this->sql = '';
    $this->whereInstanceCounter = 0;
    return $recordset;
}
   public function whereOR(Array $conditions){
    $this->sql .= ' OR ';
    $counter = 0;
    $numberOfConditions = count($conditions);//2
    foreach ($conditions as $key => $value ) {
        $arr[]=$value;
   
    }
    for($i=0;$i<=count($arr)-1;$i++){
        if($i == 2){
            $this->sql .= "?";//useremail
            $this->valueBag[] = $arr[$i];
        }
        else{
            $this->sql .= $arr[$i];
        }
    }

    return $this;
    
}
public function whereAND(Array $conditions){
    $this->sql .= ' AND ';
    $counter = 0;
    $numberOfConditions = count($conditions);//2
    foreach ($conditions as $key => $value ) {
        $arr[]=$value;
    }
    for($i=0;$i<=count($arr)-1;$i++){
        if($i == 2){
            $this->sql .= $arr[$i];//useremail
            $this->valueBag[] = $arr[$i];
        }
        else{
            $this->sql .= $arr[$i];
        }
    }
    return $this;
    
}
  
    public function insert(Array $values){
        $this->sql .= ' INSERT INTO ';
        $this->sql .= $this->tablename;
        if($this->columnCount>=1){
           
            $this->sql .= ' (';
            for($count=0;$count<$this->columnCount;$count++){
                $this->sql .= $this->columns[$count];
                if($count < ($this->columnCount-1)){
                    $this->sql .= ',';
                }
            }
            $this->sql .= ')';
        }
            
        $this->sql .= ' VALUES( ';
        $fieldCount = count($values);
        
        $this->valueBag=$values;
        
        for($count=0;$count<$fieldCount;$count++){
            $this->sql .= ' ?';
            if($count < ($fieldCount-1)){
                $this->sql .= ',';
            }
        }
        $this->sql .= ')';
        
        return $this;
    }
    public function column(Array $column){
        $this->columns = $column;
        $this->columnCount = count($this->columns);
        return $this;
    }
    public function table($tablename){
        $this->tablename= $tablename;
        return $this;

    }
    public function delete($tablename) {
        $this->tablename=$tablename;
        $this->sql = 'DELETE FROM '; 
       
        $this->sql.= $this->tablename;
        
    return $this;   
    }
    public function update(Array $value){
        $this->sql .= 'UPDATE ';
        $this->sql .= $this->tablename . ' '.'SET' ;
 
        $numberArrayElements = count ($value);

        for($counter=0; $counter < $numberArrayElements; $counter++){
            if($counter < ($numberArrayElements - 1)){
                $this->sql .= ' '.$value[$counter];
               
            }
            else{
                $this->sql .= ' \''.$value[$counter]. '\' ';
              
            }
        }
      
    
    return $this;
    }

    public function runQuery(){
        $dbStatement=$this->db->prepare($this->sql);
        $dbStatement->execute($this->valueBag);
        $dbStatement->fetchAll(PDO::FETCH_OBJ);
        
        // if($dbStatement)
        //     echo "INSERTED";
        // else
        //     echo "NOT INSERTED";    
        $this->sql = '';
        return $dbStatement;
    }

   

    public function showQuery(){
        return $this->sql;
    }
    public function showValueBag(){
        return $this->valueBag;
    }
    
    public function limit($limit)
    {
        $this->sql .= ' LIMIT '.$limit.';';
        $dbStatement =$this->db->prepare ($this->sql);
        $dbStatement->execute($this->valueBag);

        $recordset = $dbStatement->fetchAll(PDO::FETCH_OBJ);
        $this->sql = '';
        $this->whereInstanceCounter = 0;
        return $recordset;
    }
}
