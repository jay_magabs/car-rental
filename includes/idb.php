<?php

interface idb{
    public function select(Array $fieldList=null);
    public function from($table);
    public function get();
    public function getAll();
    public function column(Array $column);
    public function insert(Array $values);
    public function delete($tablename);
    public function update(Array $value);
    public function runQuery();
    public function showQuery();
    public function showValueBag();
    public function where($field,$operator,$value);
    public function whereOR(Array $conditions);
    public function whereAND(Array $conditions);
}