<?php
session_start();
include_once '../DBlibrary.php';
include_once '../requirelogin.php';
$db= new DBlibrary;
$new = new DBlibrary;



	$transac = $_SESSION['transId'];
	$username = $_SESSION['username'];
    //echo $username;
    

	$services = $db->select()->from('services')->getAll();
	
    $user = $db->select()->from('client')->where('username', '=', $username)->get();
	$userId = $user->user_id;
	//echo $userId;

	
	if(isset($_POST['submit']))
{
    // $_SESSION['serve'] = $serve;
	// $serve = $_POST['serve'];
	// echo "<br></br>";
	// echo "Service ID and Name:";	
	// echo "<br>";
	// echo $serve;
	// $sName = $db->select()->from('services')->where('service_id', '=', $serve)->get();
	$status = "pending";
	// $scheduleDate = explode('T', $time)[0];
	// $scheduleTime = explode('T', $time)[1];
	// echo "<br>";
	// echo $scheduleTime;
	// echo $scheduleDate;
	$new->table('transactions')->update(['status', '=', $status])->where('transaction_id', '=', $transac)->runQuery();
	// $insert = $db->column(['status'])->table('transactions')->insert([$status])->runQuery();
	// echo "<pre>";
	// var_dump ($insert);
    // echo "</pre>";
	$username = $_SESSION['username'];
	header('Location: order-complete.php'); 
	
}

if(isset($_POST['add']))
{
	$_SESSION['serve'] = $serve;
	$serve = $_POST['serve'];
	echo $serve;

	
	$sPrice = $db->select()->from('services')->where('service_id', '=', $serve)->get();


	$insert = $db->column(['transaction_detail_id', 'service_id', 'price'])->table('transaction_details')->insert([$transac, $sPrice->service_id, $sPrice->service_price])->runQuery();
	header('Location: service.php');
}


?>

<!DOCTYPE HTML>
<html>
	<head>
	<title>Footwear - Free Bootstrap 4 Template by Colorlib</title>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Rokkitt:100,300,400,700" rel="stylesheet">
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Ion Icon Fonts-->
	<link rel="stylesheet" href="css/ionicons.min.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.min.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="css/magnific-popup.css">

	<!-- Flexslider  -->
	<link rel="stylesheet" href="css/flexslider.css">

	<!-- Owl Carousel -->
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">
	
	<!-- Date Picker -->
	<link rel="stylesheet" href="css/bootstrap-datepicker.css">
	<!-- Flaticons  -->
	<link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="css/style.css">

	</head>
	<body>
		
	<div class="colorlib-loader"></div>

	<div id="page">
		<nav class="colorlib-nav" role="navigation">
			<div class="top-menu">
				<div class="container">
					<div class="row">
						<div class="col-sm-7 col-md-9">
							<div id="colorlib-logo"><a href="index.php">Web Car Services</a></div>
						</div>
						<div class="col-sm-5 col-md-3">
			            <form action="#" class="search-wrap">
			               <div class="form-group">
			                  <input type="search" class="form-control search" placeholder="Search">
			                  <button class="btn btn-primary submit-search text-center" type="submit"><i class="icon-search"></i></button>
			               </div>
			            </form>
			         </div>
		         </div>
					<div class="row">
						<div class="col-sm-12 text-left menu-1">
							<ul>
								<li><a href="index.php">Home</a></li>
								<li class="has-dropdown active">
                                <a href="chooselocation.php"  class="active" >Book Service</a>
									<!-- <ul class="dropdown">
										<li><a href="product-detail.php">Product Detail</a></li>
										<li><a href="cart.php">Shopping Cart</a></li>
										<li><a href="checkout.php">Checkout</a></li>
										<li><a href="order-complete.php">Order Complete</a></li>
										<li><a href="add-to-wishlist.php">Wishlist</a></li>
									</ul> -->
								</li>
								<li><a>Booked Details</a></li>
								<li><a href="about.php">About</a></li>
								

								<?php
								if(isset($_SESSION['username'])) {?>
								<li class="cart"><a href="../logout.php">Logout</a></li>
								<?php } ?>
								
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="sale">
				<div class="container">
					<div class="row">
						<div class="col-sm-8 offset-sm-2 text-center">
							<div class="row">
								<div class="owl-carousel2">
									<div class="item">
										<div class="col">
											<h3><a href="#">25% off (Almost) Everything! Use Code: Summer Sale</a></h3>
										</div>
									</div>
									<div class="item">
										<div class="col">
											<h3><a href="#">Our biggest sale yet 50% off all summer shoes</a></h3>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</nav>

		<div class="breadcrumbs">
			<div class="container">
				<div class="row">
					<div class="col">
						<p class="bread"><span><a href="index.php">Home</a></span> / <span>Book</span></p>
					</div>
				</div>
			</div>
		</div>


		<div class="colorlib-product">
			<div class="container">
				<div class="row row-pb-lg">
					<div class="col-md-10 offset-md-1">
						<div class="process-wrap">
							<div class="process text-center">
								<p><span>02</span></p>
								<h3>Dealer</h3>
							</div>
							<div class="process text-center">
								<p><span>03</span></p>
								<h3>Schedule</h3>
							</div>
                            <div class="process text-center active">
								<p><span>04</span></p>
								<h3>Service</h3>
							</div>
						</div>
					</div>
				</div>
<!--HERE IS THE INPUT-->
                <div>
			         <form action="" method="post">
				        <select name="serve" id="serve" class="form-control">
   									<?php foreach($services as $service)   {
            						// $loc = $locations->area;
            						echo "<option value='".$service->service_id."'>";
            						echo $service->service_name;
            						echo "</option>";
           							// var_dump($location->area);
                                    } ?>
                        <br></select>
							<input type="submit" name="add" class="btn btn-primary" value='Add'>			
				            <input type="submit" name="submit" class="btn btn-primary">
							
			        </form>
		        </div>
<!-- END INPUT -->
			<div class="copy">
				<div class="row">
					<div class="col-sm-12 text-center">
						<p>
							<span><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></span> 
							<span class="block">Demo Images: <a href="http://unsplash.co/" target="_blank">Unsplash</a> , <a href="http://pexels.com/" target="_blank">Pexels.com</a></span>
						</p>
					</div>
				</div>
			</div>
		</footer>
	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="ion-ios-arrow-up"></i></a>
	</div>
	
	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>
   <!-- popper -->
   <script src="js/popper.min.js"></script>
   <!-- bootstrap 4.1 -->
   <script src="js/bootstrap.min.js"></script>
   <!-- jQuery easing -->
   <script src="js/jquery.easing.1.3.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Flexslider -->
	<script src="js/jquery.flexslider-min.js"></script>
	<!-- Owl carousel -->
	<script src="js/owl.carousel.min.js"></script>
	<!-- Magnific Popup -->
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/magnific-popup-options.js"></script>
	<!-- Date Picker -->
	<script src="js/bootstrap-datepicker.js"></script>
	<!-- Stellar Parallax -->
	<script src="js/jquery.stellar.min.js"></script>
	<!-- Main -->
	<script src="js/main.js"></script>

	</body>
</html>

