<?php
session_start();
require_once 'DBlibrary.php';
$db= new DBlibrary;

if(isset($_POST['login']))
{   

    $username = $_POST['username'];
    $password = $_POST['password'];

    $_SESSION['username']= $username;
    $_SESSION['password'] = $password;



    $login= $db->select()->from('client')->where('username', '=', $username)->get();
    $hash_password = PASSWORD_VERIFY($password, $login->password);
    

    // echo $login->userType;
    // echo "<br></br>";
    
    if($hash_password) {

        $_SESSION['username'] = $username;
        $_SESSION['password'] = $password;
        header('Location: ../codes/footwear/chooselocation.php');
        
    }
      
}
// // echo $hash_password;
// echo $login->fName;
// // echo $password;
// echo "<pre>";
// var_dump($login);
// echo "</pre>";
?>


<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Mobile Car Services</title>

  <!-- Custom fonts for this template-->
  <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Custom styles for this template-->
  <link href="../css/sb-admin.css" rel="stylesheet">

</head>

<body class="bg-dark">
  <div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header">Login</div>
      <div class="card-body">
        <form action="" method="POST">
          <div class="form-group">
            <div class="form-label-group">
              <input type="text" id="inputEmail" name="username" class="form-control" placeholder="Email address" required="required" autofocus="autofocus">
              <label for="inputEmail">Username:</label>
            </div>
          </div>
          <div class="form-group">
            <div class="form-label-group">
              <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required="required">
              <label for="inputPassword">Password</label>
            </div>
          </div>
          <input type="submit" class="btn btn-primary btn-block" name="login" value= "Login">
        </form>
        <div class="text-center">
          <a class="d-block small mt-3" href="signup.php">Register an Account</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
</body>

</html>
