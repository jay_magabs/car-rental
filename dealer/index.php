<?php 

session_start();
if(isset($_SESSION['username'])) {
     header("Location: worker.php"); // redirects them to homepage
     exit; // for good measure
}

 ?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" media="screen" href="loginANDsignup.css">
    <link rel="stylesheet" type="text/css" href="styles.css">
    <script src="main.js"></script>
</head>
<body background="background-elec.png">
	<div>
		
		<div class="login-box">
			<h1>Welcome!....</h1>		
			<a href="login.php"><button class="btn">Existing</button></a>
			<a href="signup.php"><button class="btn">New</button></a>
		</div>
	</div>

</body>
</html>