<?php 

require_once '../includes/dbLibrary.php';
$db= new DBlibrary;

if(isset($_POST['register'])){
    $dealer_name = $_POST['dealer_name'];
    $username = $_POST['username'];
    $password = PASSWORD_HASH($_POST["password"], PASSWORD_DEFAULT);
    $contact = $_POST['contact'];
    $emailadd = $_POST['emailadd'];
    $area = $_POST['area'];

    $insert = $db->column(['dealer_name','username','password','contact','email','area','status'])->table('dealer')->insert([$dealer_name,$username,$password,$contact,$emailadd,$area,'pending'])->runQuery();

    echo "<script>alert('Please wait for the admin to approve your registration.')</script>";

}



 ?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sign Up!</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="loginANDsignup.css">
    <script src="main.js"></script>
</head>
<body background="login.jpg">

    <div class="login-box">
        <form method="post" action="">
        <h1> Sign up here </h1>


    <div class="textbox">
        <input type="text" name="dealer_name" placeholder="Dealer Name" required="">
    </div>

    <div class="textbox">
        <input type="text" name="username" placeholder="Username" required="">
    </div>


    <div class="textbox">
        <input type="password" name="password" placeholder="Password" required="">
    </div>

    <div class="textbox">
        <input type="text" name="contact" placeholder="Contact Number" required="">
    </div>

    <div class="textbox">
        <input type="text" name="emailadd" placeholder="Email Address" required="">
    </div>

    <div class="textbox">
        <input type="text" name="area" placeholder="Area" required="">
    </div>
   


        <!-- <div class="textbox">
        <a> First Name: </a> 
            <input type="text"> <br>
        <a> Last Name: </a>
            <input type="text"> <br>
        <a> Username: </a>
            <input type="text"> <br>
        <a> Password: </a>
            <input type="password"> <br>
        <a> Contact Number: </a>
            <input type="text"> <br>
        <a> Email Address: </a>
            <input type="text"> <br> -->
       <input class="btn" type="submit" name="register" value="Register">
   </form>
       <a href="login.php"><button class="btn">Login</button></a>
</div>
</body>
</html>