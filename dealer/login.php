<?php 

session_start();
require_once '../includes/dblib.php';
$db= new DBlibrary;

if(isset($_SESSION['username'])) {
     header("Location: worker.php"); // redirects them to homepage
     exit; // for good measure
}
// $USER = null;
//   if($_SESSION){
//     header("Location: dealer.php");
//   }


    // $username = $_POST['username'];
    // $password = $_POST['password'];

    // $_SESSION['username']= $username;
    // $_SESSION['password'] = $password;
   
if(isset($_POST['login']))
{
    
    $username = $_POST['username'];
    $password = $_POST['password'];

    
    $_SESSION['username']= $username;
    $_SESSION['password'] = $password;
    

    $login= $db->select()->from('dealer')->where('username', '=', $username)->get();
    $hash_password = PASSWORD_VERIFY($password, $login->password);
    $_SESSION['status'] = $login->status;
    $_SESSION['area']=$login->area;
    $_SESSION['name']=$login->dealer_name;
   

    // echo $_SESSION['status'];
    // echo "<br></br>";
    // echo $_SESSION['username'];

    if(!$hash_password){
      echo "<script>alert('Incorrect Password!')</script>";
    }else if($_SESSION['status'] != 'dealer'){
      echo "<script>alert('Sorry the account you entered is not a dealer.')</script>";
    }else{
        $_SESSION['username'] = $username;
        $_SESSION['password'] = $password;
        header('Location: worker.php');
    }
// echo $hash_password;
// echo $password;
// echo "<pre>";
// var_dump($login);
// echo "</pre>";
}

?>




<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" media="screen" href="loginANDsignup.css">
    <script src="main.js"></script>
</head>
<body background="login.jpg">

<form method="post" action="">
        
    <div class="login-box">
        <h1>Login</h1>

        <div class="textbox">
            <i class="fas fa-user"></i>
            <input type="text" placeholder="Username" name="username" value="" required="">
        </div>

        <div class="textbox">
            <i class="fas fa-lock"></i>
            <input type="password" placeholder="Password" name="password" value="" required="">
        </div>

        <input class="btn" type="submit" name="login" value="Sign in">
       <!-- <button type="submit" name="login" class="btn">Login</button> -->
    </div>
</form>
</body>
</html>