<?php 
session_start();

include '../includes/dblib.php';
$db = new DBlibrary;
if(!$_SESSION['username'] && !$_SESSION['password']){
	header('Location:index.php');
}if($_SESSION['status']!='dealer'){
	header('Location:logout.php');
}


$workarea = $_SESSION['area'];
echo $workarea;

if(isset($_POST['approve'])){
	$_SESSION['transacnum']=$_POST['check'];
	header('Location:approve.php');
}
if (isset($_POST['denied'])) {
	$db->table('transactions')->update(['status','=','denied'])->where('transaction_id','=',$_POST['check'])->runQuery();
	echo '<script type="text/javascript">'; 
									echo 'alert("Booked Transaction Denied");'; 
									echo 'window.location.href = "transaction.php";';
									echo '</script>';
}

if (isset($_POST['paid'])) {
	$db->table('transactions')->update(['paid','=',1])->where('transaction_id','=',$_POST['check'])->runQuery();
	echo '<script type="text/javascript">'; 
									echo 'alert("Transaction Paid");'; 
									echo 'window.location.href = "transaction.php";';
									echo '</script>';
}


// $db->table('transactions')->update(['status','=','work done'])->where('transaction_id','=',$pay->transaction_id)->runQuery();
 ?>


 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<title><?php echo $workarea; ?> Dealer</title>
	<link rel="stylesheet" href="style.css" type="text/css" media="all" />
	<script type="text/javascript">
		function sureToApprove(id){
			if(confirm("Are you sure you want to Approve this request?")){
				window.location.href ='approve.php?id='+id;
			}
		}
	</script>
</head>
<body>
<!-- Header -->
<div id="header">
	<div class="shell">
		
		<div id="top">
			<h1><a href="transaction.php">DVMA Mobile Car Services</a></h1>
			<div id="top-navigation">
				Welcome <a href="#"><strong><?php echo $_SESSION['name']; ?></strong></a>
				<span>|</span>
				<a href="#"><?php echo $workarea ?> Branch</a>
				<span>|</span>
				<a href="#">Help</a>
				<span>|</span>
				<a href="#">Profile Settings</a>
				<span>|</span>
				<a href="logout.php">Log out</a>
			</div>
		</div>

<div id="navigation">
			<ul>
			    <li><a href="worker.php"><span>Workers</span></a></li>
			    <li><a href="services.php"><span>Services</span></a></li>
			    <li><a href="transaction.php"><span>Transactions</span></a></li>
			</ul>
		</div>
	</div>
</div>

<div id="container">
	<div class="shell">
		<br />
		
		<div id="main">
			<div class="cl">&nbsp;</div>
			
			<div id="content">
				
				<div class="box">
					<!-- Box Head -->
					<div class="box-head">
						<h2 class="left">Transactions</h2>
						
					</div>
					
					<div class="table">

						<form method="post" action="">

						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<th width="13"><input type="checkbox" class="checkbox" /></th>
								<th>Transaction No.</th>
								<th>Client</th>
								<th>Schedule</th>
								<th>Status</th>
								<th width="110" class="ac">Content Control</th>
							</tr>
							<?php
								
								$pend = "pending";
								$select = $db->select()->from('transactions')->where('area','=',$workarea)->getAll();
								foreach($select as $row){
									if($row->status != 'denied'){
							?>
								
							<tr>
								<td><input type="radio" class="checkbox" name="check" value="<?php  echo $row->transaction_id?>" required=""></td>
								<td><h3><a href="#"><?php echo $row->transaction_id ?></a></h3></td>
								<td><h3><a href="#"><?php echo $row->user_id ?></a></h3></td>
								<td><h3><a href="#"><?php echo $row->scheduleDate.' '.$row->scheduleTime ?></a></h3></td>
								<td><h3><a href="#"><?php echo $row->status ?></a></h3></td>
								<?php
								if($row->status == 'pending'):	 ?>
								<td><input type="submit" name="approve" value="Approve"><input type="submit" name="denied" value="Deny"></td>
								<?php elseif($row->paid == 1) : ?>
								<td><h3 style="text-align: center;"><a href="#">Work Done</a></h3></td>
								<?php  else : ?>
								<td><input type="submit" name="paid" value="Click if Paid"></td>
								<?php endif; ?>
							
							</tr>
							<?php
								}}
							?>
						</table>
						</form>
						<!-- Pagging -->
						<div class="pagging">
							<div class="right">
								<a href="#">Previous</a>
								<a href="#">Next</a>
								<a href="#">View all</a>
							</div>
						</div>
						<!-- End Pagging -->
						
					</div>
					
					
				</div>
				<!-- End Box -->

			</div>
			<!-- End Content -->
			
			
			
			<div class="cl">&nbsp;</div>			
		</div>
		<!-- Main -->
	</div>
</div>
<!-- End Container -->

<!-- Footer -->
<div id="footer">
	<div class="shell">
	<span class="left">&copy; <?php echo date("Y-M-N");?> - DVMA Mobile Car Services</span>
	<span class="right">
		</span>
	</div>
</div>
<!-- End Footer -->
 
<!--  <div class="modal" id="modal">
    <div class="modal__content">
      <a href="#" class="modal__close">&times;</a>
      <h2 class="modal__heading">Desi Developer</h2>
      <p class="modal__paragraph">yawas</p>
    </div>
 </div>
 -->

	
</body>
</html>