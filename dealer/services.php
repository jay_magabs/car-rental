<?php 
session_start();

include '../includes/dblib.php';

if(!$_SESSION['username'] && !$_SESSION['password']){
	header('Location:index.php');
}if($_SESSION['status']!='dealer'){
	header('Location:logout.php');
}

$workarea = $_SESSION['area'];
echo $workarea;


$work = new DBlibrary;
if(isset($_POST['addworker'])){
	$worker= $_POST['add'];
	$insert = $work->column(['workerName','workArea'])->table('worker')->insert([$worker,$workarea])->runQuery();
 }

 ?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<title><?php echo $workarea; ?> Dealer</title>
	<link rel="stylesheet" href="style.css" type="text/css" media="all" />
	<script type="text/javascript">
		function sureToApprove(id){
			if(confirm("Are you sure you want to Approve this request?")){
				window.location.href ='approve.php?id='+id;
			}
		}
	</script>
</head>
<body background="login.jpg">
<!-- Header -->
<div id="header">
	<div class="shell">
		
		<div id="top">
			<h1><a href="worker.php">DVMA Mobile Car Services</a></h1>
			<div id="top-navigation">
				Welcome <a href="#"><strong><?php echo $_SESSION['name']; ?></strong></a>
				<span>|</span>
				<a href="#"><?php echo $workarea ?> Branch</a>
				<span>|</span>
				<a href="#">Help</a>
				<span>|</span>
				<a href="#">Profile Settings</a>
				<span>|</span>
				<a href="logout.php">Log out</a>
			</div>
		</div>

<div id="navigation">
			<ul>
			    <li><a href="worker.php"><span>Workers</span></a></li>
			    <li><a href="services.php"><span>Services</span></a></li>
			    <li><a href="transaction.php"><span>Transactions</span></a></li>
			</ul>
		</div>
</div>
<div id="container">
	<div class="shell">
		<br />
		
		<div id="main">
			<div class="cl">&nbsp;</div>
			
			<div id="content">
				
				<div class="box">
					<!-- Box Head -->
					<div class="box-head">
						<h2 class="left">Workers</h2>
						
					</div>
					
					<div class="table">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<th width="13"><input type="checkbox" class="checkbox" /></th>
								<th>Service ID</th>	
								<th>Service Description</th>
								<th>Service Price</th>
							</tr>
					 <?php
								
								$db = new DBlibrary;
								$select = $db->select()->from('services')->getAll();
							foreach($select as $row){
							?>
							<tr>
								<td><input type="checkbox" class="checkbox" /></td>
								<td><h3><a href="#"><?php echo $row->service_id ?></a></h3></td>
								<td><h3><a href="#"><?php echo $row->service_name ?></a></h3></td>
								<td><h3><a href=""><?php echo $row->service_price; ?></a></h3></td>
							</tr>
							<?php
								}
							?> 
						</table>

						<!-- Pagging -->
						<div class="pagging">
							<div class="right">
								<a href="#">Previous</a>
								<a href="#">Next</a>
								<a href="#">View all</a>
							</div>
						</div>
						<!-- End Pagging -->	

					</div>

					
				</div>
				<!-- End Box -->

			</div>
			<!-- End Content -->
			
			
			
			<div class="cl">&nbsp;</div>			
		</div>
		<!-- Main -->
	</div>
</div>
<!-- End Container -->


<!-- Footer -->
<div id="footer">
	<div class="shell">
	<span class="left">&copy; <?php echo date("Y-M-N");?> - DVMA Mobile Car Services</span>
	<span class="right">
			
		</span>
	</div>
</div>
<!-- End Footer -->	
</body>
</html>